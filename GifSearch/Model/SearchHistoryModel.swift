//
//  SearchHistoryModel.swift
//  GifSearch
//
//  Created by Garazd on 3/29/19.
//  Copyright © 2019 OP. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class SearchHistoryModel: Object {
    
    private enum DBAccessMode {
        case read, write
    }
    
    dynamic var id = ""
    dynamic var searchWord = ""
    dynamic var imageData = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    private static func initModelObject(searchWord: String, imageData: String) -> SearchHistoryModel {
        let date = Date()
        let modelObject = SearchHistoryModel()
        modelObject.id = "\(date.timeIntervalSince1970)"
        modelObject.searchWord = searchWord
        modelObject.imageData = imageData
        return modelObject
    }
    
    @discardableResult private static func dataBaseAccess(dbAccessMode: DBAccessMode, modelObject: SearchHistoryModel?) -> [SearchHistoryModel]? {
        let realm = try! Realm()
        if dbAccessMode == .write {
            guard let modelObject = modelObject else { return nil }
            try! realm.write  {
                realm.add(modelObject, update: true)
            }
        } else if dbAccessMode == .read {
            return Array(realm.objects(SearchHistoryModel.self))
                .sorted(by: { $0.id > $1.id })
        }
        return nil
    }
    
    static func saveSearchHistory(searchWord: String, imageData: String) -> Bool {
        if !imageData.isEmpty {
            let modelObject = initModelObject(searchWord: searchWord, imageData: imageData)
            dataBaseAccess(dbAccessMode: .write, modelObject: modelObject)
            return true
        }
        return false
    }
    
    static func getSearchHistory() -> [Any] {
        return dataBaseAccess(dbAccessMode: .read, modelObject: nil) ?? [""]
    }
}
