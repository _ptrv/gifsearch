//
//  GifTableViewCell.swift
//  GifSearch
//
//  Created by Garazd on 3/29/19.
//  Copyright © 2019 OP. All rights reserved.
//

import UIKit

class GifTableViewCell: UITableViewCell {
    
    var gifImageView: UIImageView!
    var searchTextLabel: UILabel!
    var currentRow: Int!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "GifTableViewCell")
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        gifImageView = UIImageView(frame: .zero)
        searchTextLabel = UILabel(frame: .zero)
        contentView.addSubview(gifImageView)
        contentView.addSubview(searchTextLabel)
        gifImageView.translatesAutoresizingMaskIntoConstraints = false
        searchTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            gifImageView.widthAnchor.constraint(equalToConstant: 150.0),
            gifImageView.heightAnchor.constraint(equalToConstant: 100.0),
            gifImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            gifImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        
            searchTextLabel.leadingAnchor.constraint(equalTo: gifImageView.trailingAnchor, constant: 20.0),
            searchTextLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            searchTextLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20.0)
        ])
    }
}
