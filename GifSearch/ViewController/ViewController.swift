//
//  ViewController.swift
//  GifSearch
//
//  Created by Garazd on 3/28/19.
//  Copyright © 2019 OP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var searchField: UITextField!
    private var gifTableView: UITableView!
    private var networkController = NetworkController()
    private var dataSource = [SearchHistoryModel]()
    {
        didSet {
            self.gifTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        searchField.delegate = self
        gifTableView.delegate = self
        gifTableView.dataSource = self
        fetchDataSource()
    }

    private func setupView() {
        //View
        view.backgroundColor = UIColor.white
        
        //SearchField
        searchField = UITextField(frame: .zero)
        searchField.borderStyle = .roundedRect
        searchField.returnKeyType = .done
        searchField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchField)
        
        //TableView
        gifTableView = UITableView(frame: .zero, style: .plain)
        gifTableView.rowHeight = 110.0
        gifTableView.translatesAutoresizingMaskIntoConstraints = false
        gifTableView.register(GifTableViewCell.self, forCellReuseIdentifier: "GifTableViewCell")
        view.addSubview(gifTableView)
        
        setConstraints()
    }
    
    private func setConstraints() {
        let spacing:CGFloat = 19.0
        
        //SearchField
        let searchFieldHeightConstraint = searchField.heightAnchor.constraint(equalToConstant: 44.0)
        let searchFieldTopConstraint = searchField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        let searchFieldLeadingConstraint = searchField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: spacing)
        let searchFieldTrailingConstraint = searchField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -spacing)
    
        //TableView
        let tableViewTopConstraint = gifTableView.topAnchor.constraint(equalTo: searchField.bottomAnchor, constant: spacing)
        let tableViewLeadingConstraint = gifTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        let tableViewTrailingConstraint = gifTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        let tableViewBottomConstraint = gifTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -spacing)
        
        view.addConstraints([searchFieldHeightConstraint, searchFieldTopConstraint,
                             searchFieldLeadingConstraint,searchFieldTrailingConstraint,
                             
                             tableViewTopConstraint, tableViewLeadingConstraint,
                             tableViewTrailingConstraint,tableViewBottomConstraint
        ])
    }
}

extension ViewController {
    //MARK: Controller
    private func fetchDataSource() {
        dataSource = SearchHistoryModel.getSearchHistory() as! [SearchHistoryModel]
    }
    
    private func searchImage(searchWord: String) {
        networkController.getImageData(searchWord: searchWord) { (searchWord, imageData) in
            if SearchHistoryModel.saveSearchHistory(searchWord: searchWord, imageData: imageData) == false {
                self.showAlertMessage()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.fetchDataSource()
            })
        }
    }
    
    func showAlertMessage() {
        let alertController = UIAlertController(title: "NEGARAZD,", message: "image not found!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Garazd!", style: .cancel, handler: { (garazd) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true)
    }
}

extension ViewController: UITextFieldDelegate {
    //MARK: TextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchImage(searchWord: textField.text ?? "")
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GifTableViewCell", for: indexPath) as! GifTableViewCell
        cell.currentRow = indexPath.row
        cell.gifImageView.image = nil
        let imageURL = dataSource[indexPath.row].imageData
        networkController.downloadImage(imageURL: imageURL) { (image) in
            if cell.currentRow == indexPath.row {
                DispatchQueue.main.async {
                    cell.gifImageView.image = image
                }
            }
        }
        cell.searchTextLabel.text = dataSource[indexPath.row].searchWord
        return cell
    }
}
