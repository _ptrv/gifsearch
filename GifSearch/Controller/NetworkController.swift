//
//  NetworkController.swift
//  GifSearch
//
//  Created by Garazd on 3/29/19.
//  Copyright © 2019 OP. All rights reserved.
//

import UIKit
import SwiftyJSON

class NetworkController {

    func getImageData(searchWord: String, completion: @escaping (String, String) -> Void) {
        let urlString = "\(Config.BASE_URL)\(Config.API_KEY)\(Config.PARAMETER)\(searchWord)"
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else { print(error ?? "unknown error"); return }
            do {
                let json = try JSON(data: data)
                let imageData =
                json["data"][0]["images"]["fixed_height_small_still"]["url"].stringValue
                completion(searchWord, imageData)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    func downloadImage(imageURL: String, completion: @escaping (UIImage) -> Void) {
        let urlString = imageURL
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { print(error ?? "unknown error"); return }
            guard let image = UIImage(data: data) else { return }
            completion(image)
        }.resume()
    }
}
