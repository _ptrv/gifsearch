//
//  AppDelegate.swift
//  GifSearch
//
//  Created by Garazd on 3/28/19.
//  Copyright © 2019 OP. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainVC = ViewController(nibName: nil, bundle: nil)
        window?.rootViewController = mainVC
        window?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        return true
    }
}
